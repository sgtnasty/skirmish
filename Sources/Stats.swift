/*
 This file is part of Battle.
 
 Battle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 Battle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Battle.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation


/// This class calculates stats for a battle run
public class Stats
{
    public var startTime: NSDate! = nil
    public var endTime: NSDate! = nil
    public var turns: Int = 0
    public var playerCount: Int = 0
    //public var winningPlayer: Player! = nil
    
    
    /// Determines this Hit Accuracy %
    ///
    /// - Returns: nothing right now!
    public func hitAccuracyPct() -> Float {
        return 0.0
    }
    
}
