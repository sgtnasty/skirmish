//: Playground - noun: a place where people can play
/*
 This file is part of Battle.
 
 Battle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 Battle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Battle.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

func feynman()
{
    // Feynman video
    let a:Double = 1.0
    let b:Double = 0.1
    let c:Double = a / (1.0 - b)
    print("Feynman formula: \(a) / (1.0 - \(b)) = \(c)")
}

func addPlayer(names: Names) -> Player {
    let player: Player = Player(name: names.getRandomName())
    player.RollAttributes()
    return player
}

func battlerun(player_count: Int = 2)
{
    let game = Game()
    let map = Map(x: 100, y: 100, z: 100)
    game.map = map

    var n1: Names
    n1 = Names()
    
    for _ in 1...player_count {
        game.addPlayerObj(player: addPlayer(names: n1))
    }

    for (_, player) in game.players {
        print(player.printOut())
    }
    game.battle(maxturns:500)
    print("Battle completed")
    print("")
    game.printBattleResults()
}


/// Simple function to make a list of players and show names on console.
///
/// - Parameter count: The number of players to make
func makeplayers(count: Int = 5)
{
    var n1: Names
    n1 = Names()

    for i in 1...count {
        var p: Player
        p = Player(name: n1.getRandomName())
        p.RollAttributes()
        print("P \(i): \(p.name)")
    }
}

//makeplayers(count: 10)
battlerun(player_count: 2)
