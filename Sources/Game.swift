/*
 This file is part of Battle.
 
 Battle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 Battle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Battle.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

public class Game
{
    public var players: Dictionary<String, Player>
    public var map: Map! = nil
    public var stats: Stats! = nil
    static let MAX_TURNS:Int = 10
    
    public init() {
        self.players = Dictionary<String, Player>()
    }
    
    public func playersLeftStanding() -> Int {
        var leftStanding = 0
        for (_, player) in self.players {
            if player.hitpoints > 0 {
                leftStanding += 1
            }
        }
        return leftStanding
    }
    
    public func placePlayerOnMap(player: Player) {
        if map != nil {
            let xloc: Int = Int(arc4random_uniform(UInt32(self.map!.width))) + 1
            let yloc: Int = Int(arc4random_uniform(UInt32(self.map!.length))) + 1
            //_ = Location(xloc: xloc, yloc: yloc, zloc: 0)
            player.location.x = xloc
            player.location.y = yloc
            player.location.z = 0
        }
    }
    
    /**
     Adds a player to the dict or updates existing one
     - Parameter player: the new or existing player object instance to add
     */
    public func addPlayerObj(player: Player) {
        self.players.updateValue(player, forKey: player.name)
    }
    
    /**
        Determines which players are available targets based on
        - Paramter player: the player seeking targets
        - Returns: a list of players that meet the criteria of targets for the selected player.
    */
    func potentialTargets(player: Player) -> [Player] {
        var targets = [Player]()
        
        for (_,target) in self.players {
            if (player != target) {
                if (target.hitpoints > 0) {
                    targets.append(target)
                }
            }
        }
        return targets
    }
    
    /**
     determines the "best" target based on:
     - distance
     - Parameters: player: the player to check
     */
    func determineTarget(player: Player) -> Player {
        var selected: Player?
        for (target) in self.potentialTargets(player: player) {
            // just pick the first one
            //let distance = player.location.distanceTo(
            //    targetLocation: target.location)
            //print("\tdistance to target: \(distance)")
            selected = target
        }
        return selected!
    }
    
    func attack(attacker: Player, defender: Player) -> Bool{
        let attack = attacker.ThrowAttack()
        let defence = defender.ThrowDefence()
        return (attack - defence > 0)
    }
    
    func damage(attacker: Player, defender: Player) -> Int {
        let dmg = attacker.ThrowDamage()
        defender.TakeDamage(damage: dmg)
        return dmg
    }
    
    func moveTowardsTarget(player: Player, target: Player) -> Int {
        var distance = Int(player.location.distanceTo(
            targetLocation: target.location))
        if (distance > player.range) {
            player.moveTowards(target: target.location)
        }
        distance = Int(player.location.distanceTo(
            targetLocation: target.location))
        return distance
    }
    
    func playerTakeTurn(current_player: Player) {
        let target = self.determineTarget(player: current_player)
        let distance = self.moveTowardsTarget(player: current_player, target: target)
        if (current_player.range >= distance) {
            print("\(current_player.name) attempts to hit \(target.name) at range \(distance)...")
            if (self.attack(attacker: current_player, defender: target)) {
                let damage = current_player.ThrowDamage()
                target.TakeDamage(damage: damage)
                print("    hits for \(damage), \(target.name) has \(target.hitpoints) left")
            }
            else {
                print("    miss!")
            }
        }
        else {
            print("target out of range: \(distance)")
        }
    }
    
    func performBattleRound() {
        for (_, current_player) in self.players {
            self.playerTakeTurn(current_player: current_player)
            if (self.playersLeftStanding() < 2) {
                return
            }
        }
    }
    
    func getWinner() -> Player {
        var winner:Player?
        for (_, player) in self.players {
            if (player.hitpoints > 0) {
                winner = player
            }
        }
        return winner!
    }
    
    /**
     Prints out each player final stats.
     */
    public func printBattleResults() {
        for (_, player) in self.players {
            print(player.printOut())
        }
    }
    
    public func initBattle() {
        print("Initializing Battle...")
        for (_, player) in self.players {
            self.placePlayerOnMap(player: player)
        }
    }
    
    public func battle(maxturns: Int = 100) {
        self.stats = Stats()
        self.stats.startTime = NSDate()
        print("Battle started")
        print("\(self.players.count) belligerents")
        
        var turns:Int = 0
        
        self.initBattle()
        
        // run the battle here
        while (true)
        {
            turns += 1
            print("Turn \(turns), players left standing: \(self.playersLeftStanding())")
            self.performBattleRound()
            
            if (turns >= maxturns)
            {
                print("Too many turns, aborting battle")
                break
            }
            
            if (self.playersLeftStanding() < 2) {
                let winner = getWinner()
                print("\(winner.name) has won the battle, doing \(winner.damageDone) total damage")
                break
            }
        }
        
        self.stats?.endTime = NSDate()
    }
}
