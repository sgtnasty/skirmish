/*
 This file is part of Battle.
 
 Battle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 Battle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Battle.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

public class Location
{
    public var x: Int = 0
    public var y: Int = 0
    public var z: Int = 0
    
    public init(xloc: Int, yloc: Int, zloc: Int) {
        self.x = xloc
        self.y = yloc
        self.z = zloc
    }
    
    public func distanceTo (targetLocation: Location) -> Double {
        let xdiff: Double = Double(self.x) - Double(targetLocation.x)
        let xx: Double = pow(xdiff, 2.0)
        let ydiff: Double = Double(self.y) - Double(targetLocation.y)
        let yy: Double = pow(ydiff, 2.0)
        return sqrt(xx + yy)
    }
}
