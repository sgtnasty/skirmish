/*
 This file is part of Battle.
 
 Battle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 Battle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Battle.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

public class Names
{
    var namesDb: String
    var namesArr: [String]
    
    public init() {
        
        let namesUrl = Bundle.main.url(forResource: "Names", withExtension: nil)
        
        do {
            print(namesUrl as Any)
            let raw = try String(contentsOf: namesUrl!)
            self.namesDb = raw.replacingOccurrences(of: "\n", with: " ")
            self.namesArr = self.namesDb.components(separatedBy: " ")
        }
        catch {
            self.namesDb = "None"
            self.namesArr = ["None"]
        }
    }
    
    public func getRandomName() -> String
    {
        let size = self.namesArr.count
        let index = Int(arc4random_uniform(UInt32(size)))
        return self.namesArr[index]
    }
}
