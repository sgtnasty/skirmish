/*
 This file is part of Battle.
 
 Battle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 Battle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Battle.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

public class Map
{
    public var length: Int = 0
    public var width: Int = 0
    public var height: Int = 0
    //public var playersArr: [String: Location]
    
    public init(x: Int, y: Int, z: Int) {
        self.length = x
        self.width = y
        self.height = z
        //self.playersArr = [:]
    }
    
    /*
    public func addPlayer(player: Player, loc: Location)
    {
        self.playersArr[player.name] = loc
    }
    */
}
