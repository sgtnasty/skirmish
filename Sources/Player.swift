/*
 This file is part of Battle.
 
 Battle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 Battle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Battle.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

/// Random 1d6 simulation utility function
func rolld6() -> Int {
    return Int(arc4random_uniform(6) + 1)
}

// Helper function to roll 3d6
func roll3d6() -> Int {
    return rolld6() + rolld6()
}

public class Player : Equatable
{
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    public static func ==(lhs: Player, rhs: Player) -> Bool {
        return (lhs.name == rhs.name && lhs.level == rhs.level
            && lhs.power == rhs.power)
    }

    public var name: String = ""
    public var armor: Int = 0
    public var power: Int = 0
    public var speed: Int = 0
    public var range: Int = 0
    public var attack: Int = 0
    public var defence: Int = 0
    public var level: Int = 1
    public var hitpoints = 0
    public var damageDone = 0
    public var location: Location
    
    let minAttrVal = 6
    
    public init(name: String) {
        self.name = name
        self.location = Location(xloc: 0, yloc: 0, zloc: 0)
    }
    
    private func roll_attribute() -> Int {
        return minAttrVal + rolld6() + rolld6()
    }
    
    public func RollAttributes() {
        self.armor = self.roll_attribute()
        self.power = self.roll_attribute()
        self.speed = self.roll_attribute()
        self.range = self.roll_attribute()
        self.attack = self.roll_attribute()
        self.defence = self.roll_attribute()
        self.hitpoints = self.attack + self.power - self.speed + rolld6() + rolld6()
    }
    
    public func ThrowAttack() -> Int {
        let roll = (roll3d6() + self.attack)
        return roll
    }
    
    public func ThrowDefence() -> Int {
        let roll = (roll3d6() + self.defence)
        return roll
    }
    
    public func ThrowDamage() -> Int {
        let damage = (rolld6() + self.power)
        self.damageDone += damage
        return damage
    }
    
    /// Take damage
    ///
    /// - Parameter damage: the amount of damage incomming
    public func TakeDamage(damage: Int) {
        let applied_damage = (damage - self.armor)
        if (applied_damage) > 0 {
            self.hitpoints -= applied_damage
        }
    }
    
    /// Dont know where I got this algorithm, it makes no sense
    ///
    /// - Parameters:
    ///   - target: A Location to move towards based on player speed
    public func moveTowards(target: Location) {
        let dx = Int(Double(self.speed) / 2.0)
        let dy = self.speed - dx
        
        // Move along the X axis
        if (self.location.x < target.x) { self.location.x += dx }
        else { self.location.x -= dx }
        
        // Move along the Y axis
        if (self.location.y < target.y) { self.location.y += dy }
        else { self.location.y -= dy }
    }
    
    public func printOut() -> String {
        return "\(self.name)/\(self.level) \(self.attack)/\(self.defence) \(self.hitpoints)hp {Ar=\(self.armor), Po=\(self.power), Sp=\(self.speed), Ra=\(self.range)}"
    }
}
